$version = '5.9.4'
$appdir = '/bamboo/bamboo-home'

file {'/bamboo':
  ensure => directory
}
-> class {'bamboo':
  version      => $version,
  installdir   => "/bamboo/${version}",
  homedir      => '/home/bamboo/',
  uid          => 2479,
  gid          => 2479,
  appdir       => $appdir,
  jvm_xms      => '1536m',
  jvm_xmx      => '2048m',
  jvm_permgen  => '256m',
  jvm_opts     => '-Djavax.net.ssl.trustStore=/bamboo/bamboo-home/security/cacerts',
  jvm_optional => [ '-Dcom.sun.management.jmxremote',
                    '-Dcom.sun.management.jmxremote.port=8686',
                    '-Dcom.sun.management.jmxremote.authenticate=false',
                    '-Dcom.sun.management.jmxremote.ssl=false',
                    '-Dcom.sun.management.jmxremote.local.only=false',
                    '-Dfile.encoding=UTF-8',
                    '-XX:+UseG1GC',
                    '-XX:+UnlockExperimentalVMOptions' ]
}
-> file {'/bamboo/current':
  ensure => link,
  target => $appdir
}
